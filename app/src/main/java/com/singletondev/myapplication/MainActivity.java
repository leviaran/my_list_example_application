package com.singletondev.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    private List<ModelData> listString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.list_view_item);

        listString = new ArrayList<ModelData>();
        listString.add(new ModelData("Sanrio Puroland","https://img.jalan2kejepang.com/uploads/2016/05/Sanrio-Puroland.jpg"));
        listString.add(new ModelData("Chidorigafuchi","https://img.jalan2kejepang.com/uploads/2017/10/Tempat-Wisata-Rekomendasi-Melihat-Sakura-Chidorigafuchi-768x512.jpg"));
        listString.add(new ModelData("Gunung Takao","https://img.jalan2kejepang.com/uploads/2016/12/Pintu-Masuk-Takaosan.jpg"));
        listString.add(new ModelData("Bunkyou_Koishikawa","https://img.jalan2kejepang.com/uploads/2016/11/Bunkyou_Koishikawa_Botanical_Japanese_Garden.jpg"));
        listString.add(new ModelData("Edo Tokyo","https://img.jalan2kejepang.com/uploads/2016/12/Edo-Tokyo-Open-Air-Museum-di-Jepang-768x576.jpg"));

        list_adapter adapter = new list_adapter(getBaseContext(),listString);
        listView.setAdapter(adapter);

        //add intent object
        final Intent sendIntent = new Intent();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,listString.get(i).getItemNama());
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });


        new Data(new Data.DataCallback() {
            @Override
            public void onRespons(String json) {
                Log.e("json",json);
                GitData gitData = new GitData();

                try{
                    gitData.getDataGit(json);

                }catch (JSONException jsonExc){
                    jsonExc.printStackTrace();
                }
            }

            @Override
            public void onError(Throwable throwable) {
                Log.e("Error",throwable.getMessage());
            }
        }).Process();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_item_res,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu1:
                Toast.makeText(getBaseContext(),"ini menu satu",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.menu2:
                Toast.makeText(getBaseContext(),"ini menu dua",Toast.LENGTH_LONG).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

}
