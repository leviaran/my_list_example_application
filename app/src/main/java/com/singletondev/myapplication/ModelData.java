package com.singletondev.myapplication;

/**
 * Created by Randy Arba on 11/12/17.
 * This apps contains MyApplication
 * @email randy.arba@gmail.com
 */

public class ModelData {
    private String itemNama;
    private String itemBackground;

    ModelData(String itemNama, String itemBackground){
        this.itemNama = itemNama;
        this.itemBackground = itemBackground;
    }

    public String getItemNama() {
        return itemNama;
    }

    public void setItemNama(String itemNama) {
        this.itemNama = itemNama;
    }

    public String getItemBackground() {
        return itemBackground;
    }

    public void setItemBackground(String itemBackground) {
        this.itemBackground = itemBackground;
    }
}
