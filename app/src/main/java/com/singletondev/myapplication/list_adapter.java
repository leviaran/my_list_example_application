package com.singletondev.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Randy Arba on 11/10/17.
 * This apps contains MyApplication
 * @email randy.arba@gmail.com
 */

public class list_adapter extends BaseAdapter {

    private Context context;
    private List<ModelData> kata;
    private LayoutInflater layoutInflater;

    list_adapter (Context context, List<ModelData> kata){
        this.context = context;
        this.kata = kata;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return kata.size();
    }

    @Override
    public Object getItem(int i) {
        return kata.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View rowView = layoutInflater.inflate(R.layout.item_view,viewGroup,false);
        TextView textView = rowView.findViewById(R.id.item_text);
        ImageView imageView = rowView.findViewById(R.id.item_image);
        textView.setText(kata.get(i).getItemNama());
        Picasso.with(context).load(kata.get(i).getItemBackground()).into(imageView);
        return rowView;
    }

    @Override
    public CharSequence[] getAutofillOptions() {
        return new CharSequence[0];
    }
}
