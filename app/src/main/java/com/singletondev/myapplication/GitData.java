package com.singletondev.myapplication;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Randy Arba on 11/11/17.
 * This apps contains MyApplication
 * @email randy.arba@gmail.com
 */

public class GitData {
    private String login;
    private Integer id;
    private String avatar_url;

    public void getDataGit(String json) throws JSONException{
        final String ITEM = "items";
        final String OWNER = "owner";
        final String LOGIN = "login";
        final String ID = "id";
        final String AVATAR_URL = "avatar_url";

        try {

            String total;
            JSONObject gitJson = new JSONObject(json);
            total = gitJson.getString("total_count");
            Log.e("total",total);
            JSONArray item = gitJson.getJSONArray("items");

            for (int i=0;i<item.length();i++){
                String login;
                Integer id;
                String avatar_url;

                JSONObject getJsonObjectOwner = item.getJSONObject(i);
                login = getJsonObjectOwner.getString("id");

                Log.e("login ID",login);

            }

        } catch (JSONException e){
            Log.e("error",e.getMessage());
        }
    }
}
