package com.singletondev.myapplication;

import android.net.Uri;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Randy Arba on 11/11/17.
 * This apps contains MyApplication
 * @email randy.arba@gmail.com
 */

public class Data {
    public interface DataCallback{
        void onRespons(String json);
        void onError(Throwable throwable);
    }

    private DataCallback dataCallback;

    Data(DataCallback dataCallback){
        this.dataCallback = dataCallback;
    }

    public void Process(){
        String url = BuildUrl();
        new DataProcess().execute(url);
    }

    public String BuildUrl(){
        String url = "https://api.github.com/search/repositories?";
        String query_param = "q";
        String sort_param = "sort";

        String format = "json";

        Uri uri = Uri.parse(url).buildUpon()
                .appendQueryParameter(query_param,"android")
                .appendQueryParameter(sort_param,"stars")
                .build();

        return uri.toString();
    }

    public class DataProcess extends AsyncTask<String,String,String>{

        HttpURLConnection urlConnection;
        BufferedReader reader;
        String json;

        @Override
        protected String doInBackground(String... strings) {
            try {
                URL urls = new URL(strings[0]);
                urlConnection = (HttpURLConnection) urls.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();

                if (inputStream == null){
                    return null;
                }

                reader = new BufferedReader(new InputStreamReader(inputStream));
                String line;

                while ((line = reader.readLine()) != null){
                    buffer.append(line + "\n");
                }

                json = buffer.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException ex){
                ex.printStackTrace();
            }
            return json;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dataCallback.onRespons(s);
        }
    }

}
